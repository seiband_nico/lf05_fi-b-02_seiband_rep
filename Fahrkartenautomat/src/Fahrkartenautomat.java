﻿import java.util.Scanner;

class Fahrkartenautomat
{
    
private static Scanner bedienfeld = new Scanner(System.in);   
private static double eingezahlterGesamtbetrag;
private static double eingeworfeneMünze;
private static double rückgabebetrag;
private static double zuZahlenderBetrag;

    // Eingabe der gewünschten Kartenanzahl
    //-------------------------------------
    public static void karten() {
    
    	double eingabeKarten;
    	    	
        System.out.print("Wie viele Karten möchten Sie? " );
        eingabeKarten = bedienfeld.nextDouble();
    	zuZahlenderBetrag = eingabeKarten * 1.50;
    	System.out.printf("Der zu zahlende Betrag beträgt (EURO): %.2f\n", zuZahlenderBetrag );
    	bezahlen();
    }
    
    // Geldeinwurf
    public static void bezahlen() {
    	
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
    		System.out.printf("\nNoch zu zahlen%.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    		System.out.print("Eingabe (min. 5Ct, max 2€):");
    		
    		eingeworfeneMünze = bedienfeld.nextDouble();
        	if (eingeworfeneMünze < 0.05 || eingeworfeneMünze > 2.00) {
        		System.out.println("\nKleiner als 5 Cent oder größer als 2 € nicht möglich!");
        		System.out.println("Bitte noch einmal versuchen!");
        		System.out.printf("\nNoch zu zahlen: %.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
        		System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro):");
        		eingeworfeneMünze = bedienfeld.nextDouble();
        		
        		if (eingeworfeneMünze < 0.05 || eingeworfeneMünze > 2.00) {
        			System.out.println("\nKleiner als 5 Cent oder größer als 2 € nicht möglich!");
        			System.out.print("Abbruch!!");
        			System.exit(0);
        		}
        		else eingezahlterGesamtbetrag += eingeworfeneMünze;
        	}
        	else eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	fahrscheinAusgabe();
    }
	
    	public static void fahrscheinAusgabe() {
    
    	// Fahrscheinausgabe
    	//------------------
    		System.out.println("\nFahrschein wird ausgegeben");
    		for (int i = 0; i < 8; i++)
    		{
    			System.out.print("=");
    			try {
    					Thread.sleep(250);
    						
    				} catch (InterruptedException e) 
    			{
    				
    				e.printStackTrace();
    			}
    		}
    		geldRueckgabe();	
    	}
    
    public static void geldRueckgabe() {
    	
    	
    	
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    
    	if(rückgabebetrag >= 0.00) 
    	{
    		System.out.printf("Der Rückgabebetrag in Höhe von %.2f\n", rückgabebetrag , "€");
    		System.out.println("wird in folgenden Münzen ausgezahlt:");
    		
    		while(rückgabebetrag >= 2.00) { // 2€-Münzen
    			
    			System.out.println("2€");
    			rückgabebetrag -= 2.00;
    		}
    		while(rückgabebetrag >= 1.00) { // 1€-Münzen
    			
    			System.out.println("1€");
    			rückgabebetrag -= 1.00;
    		}	
    		while(rückgabebetrag >= 0.50) { // 50ct-Münzen
    			
    			System.out.println("50ct");
    			rückgabebetrag -= 0.50;
    		}
    		while(rückgabebetrag >= 0.20) { // 20ct-Münzen
    			
    			System.out.println("20ct");
    			rückgabebetrag -= 0.20;
    		}
    		while(rückgabebetrag >= 0.10) { // 10ct-Münzen
    			
    			System.out.println("10ct");
    			rückgabebetrag -= 0.10;
    		}
    		while(rückgabebetrag >= 0.05) { // 5ct-Münzen
    			
    			System.out.println("5ct");
    			rückgabebetrag -= 0.05;
    		}
    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n vor Fahrtantritt entwerten zu lassen!\n Wir wünschen Ihnen eine gute Fahrt.");
    	}

    	
    }

    public static void main(String[] args)
    {     
    	karten();
       
    }












}